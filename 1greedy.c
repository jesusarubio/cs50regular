/*
	Context for the code:

	http://cdn.cs50.net/2015/x/psets/1/pset1/pset1.html#time_for_change

*/

#include <stdio.h>
#include <math.h>

void greedy(void);
void greedy2(void);

int main(void)
{
	greedy();
	return 0;
}

/*
	Implements a greedy algorithm with substraction and additions
*/

void greedy(void)
{
	float inputUser;
	int quarter = 25;
	int dime = 10;
	int nickel = 5;
	int penny = 1;
	int coin;

	do
	{
		printf("How much change do I owe you? ");
		scanf("%f", &inputUser);
	}
	while(inputUser < 0.00);

	inputUser = inputUser * 100;

	int change;
	change = round(inputUser);

	while (change >= quarter)
	{
		change = change - quarter;
		coin++;
	}

	while (change >= dime)
	{
		change = change - dime;
		coin++;
	}

	while (change >= nickel)
	{
		change = change - nickel;
		coin++;
	}	
	
	while (change >= penny)
	{
		change = change - penny;
		coin++;
	}

	printf("%i\n", coin);
}

/*
	Implements the greedy algorithm with Modulo, Division and Addition operations
*/

void greedy2(void)
{
	float inputUser;
	int quarter = 25;
	int dime = 10;
	int nickel = 5;
	int penny = 1;
	int coin;
	int change;

	do
	{
		printf("How much change do I owe you? ");
		scanf("%f", &inputUser);
	}
	while(inputUser < 0.00);

	inputUser = inputUser * 100;

	
	change = round(inputUser);

	while (change >= quarter)
	{
		coin = coin + (change - (change % quarter)) / quarter;
		change = change % quarter;
	}
	
	while (change >= dime)
	{
		coin = coin + (change - (change % dime)) / dime;
		change = change % dime;
	}

	while (change >= nickel)
	{
		coin = coin + (change - (change % nickel)) / nickel;
		change = change % nickel;
	}

	while (change >= penny)
	{
		coin = coin + (change - (change % penny)) / penny;
		change = change % penny;
	}
	printf("%i\n", coin);
}