/*
	Context for the code:

	http://cdn.cs50.net/2015/x/psets/1/pset1/pset1.html#itsa_mario
*/

#include <stdio.h>

void mario(void);
void givemecount(void);

int counter;

int main(void)
{	
	givemecount();
	mario();
	return 0;
}

void givemecount(void)
{

	/*
		Prompts the user for an integer between 0 and 23
		in order to print the Pyramid. Repeats the loop
		if the user provides a non valid argument.
	*/
	
	do
	{
		printf("Give me the pyramid height: ");
		scanf("%d", &counter);
	}
	while (counter < 0  || counter > 23);
}

void mario(void)
{
	
	/*
		Prints into the terminal the pyramid from Super Mario's
		end screen.
	*/

	int column;
	int row;
	
	for (row = 0; row < counter; row++)
	{
		for (column = 0; column <= counter; column++)
		{
			if ( row < counter - column - 1)
			{
				printf(" ");
			}

			else
			{
				printf("#");
			}
		}

		printf("\n");
	}
}