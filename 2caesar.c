/*
	Context for the code: 

	http://cdn.cs50.net/2015/x/psets/2/pset2/pset2.html#hail_caesar

	Implementation of the Caesar Cypher en.wikipedia.org/wiki/Caesar_cipher/
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[])
{
	int key = atoi(argv[1]);

	/* 
		This loop checks if when the program is compiled, a key has been provided
		In case it hasn't, prints an error message and kicks out the user.
	*/

	if (argc == 2)
	{
		char* toencrypt;
		toencrypt = malloc(sizeof(toencrypt) + 1); 
		printf("Provide me the phrase you want to encrypt, please: ");
		fgets(toencrypt, 1001, stdin);
		
		for (int count = 0; count < strlen(toencrypt); count++)
		{
			// Cyphers the lowercase characters
			if (toencrypt[count] >= 'a' && toencrypt[count] <= 'z')
			{
				toencrypt[count] = ((toencrypt[count] - 'a') + key) % 26 + 'a';
			}
			// Cyphers the uppercase characters
			else if (toencrypt[count] >= 'A' && toencrypt[count] <= 'Z')
			{
				toencrypt[count] = ((toencrypt[count] - 'A') + key) % 26 + 'A';
			}
			// Transfer the non alphanumeric Characters
			else
			{
				toencrypt[count] = toencrypt[count];
			}
		}

		printf("%s\n", toencrypt);

		return 0;
	}
	else
	{
		printf("Just one key, you dummy!\n");
		return 1;
	}
}