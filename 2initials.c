/*
	Context for the code:

	http://cdn.cs50.net/2015/x/psets/2/pset2/pset2.html#initializing

	This C program selects the initials for the string provided and prompts the user
	back with them in capitals
*/

#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
	int stringCount;
	int charCount;

	for (stringCount = 1; stringCount < argc; stringCount++)
	{
		for (charCount = 0; charCount < strlen(argv[stringCount]); charCount++)
		{
			if ((argv[stringCount][charCount]>= 65 && argv[stringCount][charCount] <= 90) && charCount == 0)
			{
				printf("%c", argv[stringCount][charCount]);
			}

			else if ((argv[stringCount][charCount] >= 97 && argv[stringCount][charCount]<= 122) && charCount == 0)
			{
				printf("%c", argv[stringCount][charCount] - 32);
			}
		}

	}
	printf("\n");

	return 0;
}