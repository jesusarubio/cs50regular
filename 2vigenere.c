/*
	Context for the code: 

	http://cdn.cs50.net/2015/x/psets/2/pset2/pset2.html#parlez_vous_fran%C3%A7ais

	Implementation of the Vigenere Cypher en.wikipedia.org/wiki/Vigenere_cipher/
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char* argv[])
{
	/*
		This loop checks if when the program is compiled, a key has been provided
		In case it hasn't, prints an error message and kicks out the user.
	*/

	if (argc == 2)
	{
		char* toencrypt;
		toencrypt = malloc(sizeof(toencrypt) + 1); 
		printf("Provide me the phrase you want to encrypt, please: ");
		fgets(toencrypt, 1001, stdin);
		char* key = argv[1];
		int keyLength = strlen(key);
		int keyCount = 0;
		
		/*
			This for clause makes sure that the key provided includes only alphanumeric
			characters.
		*/

		for (int count = 0; count < strlen(argv[1]); count++)
		{
			if (!isalpha(argv[1][count]))
			{
				printf("The key must only contain letters!\n");
				return 1;
			}
		}
		
		/*
			To make the implementation easier, converts all the characters of the
			key into lowercase characters
		*/

		for (int count = 0; count < keyLength; count++)
		{
			key[count] = tolower(key[count]);
			key[count] = key[count] - 'a';
		}

		for (int count = 0, cipherLength = strlen(toencrypt); count < cipherLength; count++)
		{
			// Cyphers the lowercase characters
			if (isupper(toencrypt[count]))
			{
				toencrypt[count] = (toencrypt[count] - 'A' + key[keyCount%keyLength]) % 26 + 'A';
			}
			// Cyphers the uppercase characters
			if (islower(toencrypt[count]))
			{
				toencrypt[count] = (toencrypt[count] - 'a' + key[keyCount%keyLength]) % 26 + 'a';
			}
			// Transfer the non alphanumeric Characters
			else
			{
				toencrypt[count] = toencrypt[count];
			}

			keyCount++;
		}

		printf("%s\n", toencrypt);

		return 0;
	}

	else
	{
		printf("Just one key, you dummy!\n");
		return 1;
	}
}