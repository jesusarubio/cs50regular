/*
	Context for the code:

	http://cdn.cs50.net/2015/x/psets/3/pset3/pset3.html

	Implementation of an insertion sort and a binary search algorithms
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Declaration of prototypes for the Sorting and search Algorithms
void insertionSort(int values[], int n);
bool binarySearch(int value, int values[], int n);

int main(int argc, char* argv[])
{
	int values[argc - 1];
	for (int i = 0; i < argc - 1; ++i)
	{
		values[i] = atoi(argv[i+1]);
	}

	insertionSort(values, argc);

	// Just for the sake of sanity, print to the terminal the values organized
	for (int j = 0; j < argc - 1; ++j)
	{
		printf("+%i ", values[j]);
	}

	int value;
	printf("Provide me with the value to search: ");
	scanf("%i", &value);

	if (binarySearch(value, values, argc) == true)
	{
		printf("TRUE!\n");
	}
	else
	{
		printf("FALSE!\n");
	}

	return 0;
}

/*
	Sorts array of n values (Implementation of an O(n^2) sorting algorithm)
*/

void insertionSort(int values[], int n)
{
	for (int i = 1; i < n - 1; i++)
	{
		int element = values[i];
		int j = i;

		while (j > 0 && values[j-1] > element)
		{
			values[j] = values[j-1];
			j = j-1;
		}

		values[j] = element;
	}
}

/*
	Returns true if value is in array of n values, else false.
*/

bool binarySearch(int value, int values[], int n)
{
	int min = 0;
	int max = n;


	while (min < max)
	{
		int mid = (min + max)/2;

		if (values[mid] == value)
		{
			return true;
		}

		else if (values[0] == value)
		{
			return true;
		}

		else if (values[mid] < value)
		{
			min = mid + 1;
		}

		else
		{
			max = mid - 1;
		}
	}

	return false;
}
